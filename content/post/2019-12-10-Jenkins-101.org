#+TITLE: Jenkins 101
#+DATE: 2019-12-10

*** What is Jenkins?
[[https://en.wikipedia.org/wiki/Continuous_integration][CI]] provides automates parts of software development.
When a developer makes new software, automated /pipelines/ can run multiple /steps/.
The steps could be for ensuring quality with linting and tests, 
or it could be to build the project to make binary
executable files, or both.

Jenkins is a tool for Continuous Integration (CI). To use Jenkins we use it's web-interface,
to create, modify, start/stop and monitor "jobs".

*** How to start?
Generate a SSH key pair in your system. Here is a [[https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key][good guide]]. Make sure you don't use your primary keys, but create new ones.

Add your SSH public key in your Git server, which is Github in my case.
Login to your Jenkins instance.

In the left sidebar, you'd see an icon for /new item/.

#+ATTR_HTML: :width 200px
[[./jenkins1.png]]

Type in your project name and select "Pipeline". There are many other options, but I found this option best to get started.

#+ATTR_HTML: :width 500px
[[./jenkins2.png]]

You will be taken to your new Project's configuration file. There are many options. You can fill the title, description, etc. Then scroll down to see section "Pipelines".

#+ATTR_HTML: :width 500px
[[./jenkins3.png]]

Select the definition "Pipeline script from SCM" and select SCM as "git".
Click on add credentials. Select "SSH Username with private key".

#+ATTR_HTML: :width 500px
[[./jenkins4.png]]

You're done with the Jenkins web interface configuration!

It is not finished yet. =Jenkinsfile= needs to be added to the git repo. Continued in the next post!
