#+TITLE: Delete your Dockerfile, fatten your Jenkinsfile!
#+DATE: 2019-12-12

While docker is vital in order to containerize your application,  using =Dockerfile=  is not required.
With my short experience of using Docker, I'd say it better not to have a =Dockerfile=!
Put all the steps in the =Jenkinsfile=.

Why?
1. =Groovy= > =Dockerfile= syntax (die exec form 💀)
2. Expose intermediate steps to your CI.
3. Shorter, cleaner code. Commands such as =COPY . .= are not required as your Jenkins pipeline takes care of it.
4. Lesser files to maintain. Keeps all the integration commands within the =Jenkinsfile=
5. Agent =docker= in the Jenkins pipeline is a neater way of doing things.
6. Blocks such as =environment {}= make it nice and easy to setup environmental variables for the whole pipeline or section.
# In many situations we use some sort of CI along with Docker, like Jenkins, or even orchestration with k8s.

I can go on, but I'd rather stop.
When you create a Dockerfile, you are adding layers on top of the existing image mentioned with the =FROM= command.
These layers or =steps= in Docker language, are built by creating temporary containers and can add complexity.
It also blackboxes the process to higher level processes.

I'm not saying that =Dockerfiles= are unnecessary... I just think that you don't need them when you can do all your work
through a =Jenkinsfile= in a shorter, cleaner way.
