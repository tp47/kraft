#+TITLE: Unit tests in Go
#+DATE: 2019-10-29

Go provides the package =testing= for making
it simple to write tests. Here is a example 
syntax.
#+BEGIN_SRC go
import "testing"

func TestAbc(t *testing.T) {
    t.Error() // to indicate test failed
}
#+END_SRC

The [[https://www.golang-book.com/books/intro/12][golang book]] describes the Go way of tests:
#+BEGIN_QUOTE
The =go test= command will look for any tests in any
of the files in the current folder and run them. 
Tests are identified by starting a function with 
the word =Test= and taking one argument of type =*testing.T=.
#+END_QUOTE
*** A simple test case

Here is the case to test if X wins when X has the
diagonal "win line" on the tic-tac-toe board.

#+BEGIN_SRC go
package core

import (
	"testing"
)

func TestCalcWinner(t *testing.T) {
	myBoard := Board{
		[9]string{
			"X", " ", " ",
			" ", "X", " ",
			" ", " ", "X",
		},
	}
	winner := calcWinner(myBoard)

	if winner != "X" {
		t.Error("The winner should be X, not ", winner)
	} else {
		t.Log("Test passed. X has won!")
	}
}
#+END_SRC

**** Lessons learnt
The test runner won't test unless:
 1. Test functions must start with =Test= in the function name.
 2. First word after =Test= in the function name should be title case. Eg. =TestDivision=.
 3. Tests should be in a file which who's name ends with =_test.go=

*** Generalizing tests
Last weekend in Passau, Eugene also showed me how to write
more general test cases and also introduced me to =testing.Run=.

Let rewrite the above test as follows:

#+BEGIN_SRC go
func TestCalcWinner(t *testing.T) {
	testset := []struct {
		desc   string
		board  Board
		winner string
	}{
		{
			"X diagonal win",
			Board{[9]string{
				"X", " ", " ",
				" ", "X", " ",
				" ", " ", "X"}},
			"X",
		},
	}

	for _, tcase := range testset {
		t.Run(tcase.desc, func(t *testing.T) {
			winner := calcWinner(tcase.board)
			if winner != "X" {
				t.Error("The winner should be X, not ", winner)
			}
		})
	}
}
#+END_SRC

So much bloat! For what? Well, now we can add any number of tests
to the array =testset= now.
=testset= is assigned to an array of anonymous =struct=s and
it's constituent variables (and datatype) is mentioned.

#+BEGIN_SRC go
	testset := []struct {
		desc   string
		board  Board
		winner string
	}{
		{
			"X diagonal win",
			Board{[9]string{
				"X", " ", " ",
				" ", "X", " ",
				" ", " ", "X"}},
			"X",
		},
		{
			"X horizontal win",
			Board{[9]string{
				"X", "X", "X",
				" ", " ", " ",
				" ", " ", " "}},
			"X",
		},
		// add more cases here
	}
#+END_SRC

Hmm, is there a way to stop writing =Board{[9]string{"X", ...}}= for every case?
Maybe, find out next post? ;)
