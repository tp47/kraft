#+TITLE: Software Project structure
#+DATE: 2019-10-28

For any software project, naming (and creation) of files and directories
provide structure. 
*** ProjStruct Kata
Today's Kata with Dejan was about project structures.
He told me that there are two ways to group things:
1. File types: Files of same type are in a group. Like =css=, =js=, ...
2. Feature: Files of the same feature are in the same group. Like =models=, =views=, or even =user_scoreboard=, ...

*** Common project structures
A common convention for the top level directories in a project is to have these folders:
#+BEGIN_SRC bash
project_name
	  /bin
	  /src
	  /libs
	  README.md
#+END_SRC

=src= contains all the source files of the project, =bin= contains the compiled binaries and
=libs= contains the shared libraries that are utilized by the project.
Unfortunately, many languages and tools have their own conventions.
This is because these are trying to solve different kinds of problems, thus
require different formats.

*** Go project structures
Go standards specify [[https://github.com/golang-standards/project-layout][project-layout]]s. Here is how a project could look
#+BEGIN_SRC bash
project_name
	  /cmd
	  /pkg
	  /docs
	  /web
	  /website
	  /vendor
	  README.md
#+END_SRC

There are several more directories which are possible, which you see in the
[[https://github.com/golang-standards/project-layout][project-layout]] repo. =cmd= provides ways to run your project on the command-line.
It should not contain the core/common functionality, which should reside in =pkg=.
Most of the project will reside in =pkg= and can be imported by other projects too.
=web= provides a web interface, =website= is the website of the project,
=vendor= contains dependencies and =docs= contains documentation.

[[https://gitlab.com/tp47/babel_tictactoe/tree/master/go/tictactoe][Babel_tictactoe]]'s go flavor has been updated to this project structure.

*** GOPATH problems
Go uses an env variable called =GOPATH=. It describes the folder where all the 
go sources of the user resides. This is a reason why =src= folders don't exsist
in go projects. The default =GOPATH= is =/home/{USERNAME}/go=.

The problem is that I cant keep the different implementations of TicTacToe
in my babel_tictactoe repo folder. Although =GOPATH= has been deprecated,
my IDE requires the project to be in that path to work normally.
Using a symbolic link from the repo's folder to the =GOPATH= did not work,
as it is not supported.

Any ideas?
