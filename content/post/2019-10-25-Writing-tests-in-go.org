#+TITLE: Go packages
#+DATE: 2019-10-26
*** Preface
  One of the people who convinced me to learn Go is [[https://github.com/zitryss/facemash][Eugene]].
  We know each other from Uni and like occasionally pair program.
  It was interesting to meet him and code in Passau, like the "old" times.
  He gave me a quick tour of Go packages.
  # I realized that I had all my logic in a single file and wanted to split it.
  # I also wanted to create directories and put different source files in different
  # folders.
  # I was a complete disaster at it, as I seemed to be using the Python way of
  # handling packages and didn't really get Go's way of doing things.
  # The last I had worked on the project was on a train, thus without internet
  # access. Eugene showed me the way by demonstrating the simple way Go uses to create
  # packages. 

*** What is a package
Source files which serve the same feature are usually grouped as a package.
The are present in a folder also named after the feature.
At the start of every Go source file, one has to define which package it belongs to.
*** The =main= package

Also, every project requires a =main= package.
It provides the entry point to a particular package.
The =main= method is also present in the same package.
The contents of =tictactoe/cmd/tictactoe/main.go= is:
#+BEGIN_SRC go
package main

import "tictactoe/pkg/core"

func main() {
	core.Game()
}
#+END_SRC

Hang on, line 3 will make sense soon!
*** How to create a package
In order to create your own packages, it is convention to
name your folder based on the package.

The current structure of the tictactoe project is like this:

#+BEGIN_SRC bash
tictactoe
        /pkg
          /core
        /cmd
          /tictactoe
#+END_SRC

Under the project, there are two high level packages.
These are =pkg= and =cmd=. Inside =pkg= is the package
=core=, which holds the core logic of the program.
=cmd= has the package =tictactoe=. Since =cmd= provides the
CLI which only works as tic-tac-toe, we may as well name
it =tictactoe=. Thus the file =tictactoe/cmd/tictactoe/main.go=
has this at the start of the file:
#+BEGIN_SRC go
package tictactoe
#+END_SRC


*** How to import a package
In order to import a package, we use mention the path
of the package's directory.
For example, in order for the file =tictactoe/cmd/tictactoe/main.go=
to access the game logic present in the file =tictactoe/pkg/core/game.go=,
we can write this:

#+BEGIN_SRC go
import "tictactoe/pkg/core"
#+END_SRC
