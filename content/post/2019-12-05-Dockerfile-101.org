#+TITLE: Dockerfile 101
#+DATE: 2019-12-05

Docker lets one to containerize their apps.
To get started, create a =Dockerfile= in the root of your project.
It can be at a different location too, but it's convention to keep
it at the root of your project.

** Minimal example
Here is a minimal example which specifies which "base image" to
use and what command to run:

#+BEGIN_SRC Dockerfile
FROM alpine
RUN uname -mrs
#+END_SRC

The =FROM= command uses an image specified in DockerHub.
In this case its the image the tag "alpine", which is
[[https://www.alpinelinux.org/][Alpine linux]]. It is a lightweight, secure, Linux distro.

The =RUN= command runs something inside your container.
=uname -mrs= is a common command to find out about the Linux
version and the distro you are running on.

You can use the command =docker build .= to make an image
for your container.
#+BEGIN_EXAMPLE
Sending build context to Docker daemon  40.45kB
Step 1/2 : FROM alpine
 ---> 965ea09ff2eb
Step 2/2 : RUN uname -mrs
 ---> Running in 0cd2b0a2571a
Linux 5.3.11-1-MANJARO x86_64
Removing intermediate container 0cd2b0a2571a
 ---> 114f3744b3ea
Successfully built 114f3744b3ea
#+END_EXAMPLE

Each =Dockerfile= command corresponds to a "Step" in the output.

Step 1, Docker looks for the base image. In this case, it already had a
copy of it. =965ea09ff2eb= is the =imageID= of the alpine image.

Step 2, it has created a temporary container with the ID
=0cd2b0a2571a=. Next, we can see the output of the =uname= command: 
=Linux 5.3.11-1-MANJARO x86_64=. What is this now? Didn't we choose
Alpine Linux as our distro? Yes, but the container still uses your host OS.

[[./docker.png]]

** Dockerizing TicTacToe

Let's /Dockerize/ my golang tictactoe app.
#+begin_src Dockerfile
FROM golang:latest
WORKDIR /tictactoe/
COPY . .
RUN go build -o ttt_api api/tictactoe/main.go
CMD ./ttt_api
#+end_src

=WORKDIR= specifies what directory inside the container docker should use
for our project. =COPY= command copies files from the host machine to the image.
=CMD= command provides defaults for an executing container.
When using =docker build=, you can use the flag =-t= to provide a tag to the image.
When I run =docker build -t tictactoe .=, this is the output:

#+BEGIN_EXAMPLE
docker build -t tictactoe .
Sending build context to Docker daemon  20.93MB
Step 1/5 : FROM golang:latest
 ---> a2e245db8bd3
Step 2/5 : WORKDIR /tictactoe/
 ---> Running in b337ec8158a2
Removing intermediate container b337ec8158a2
 ---> 5665a21d71df
Step 3/5 : COPY . .
 ---> 3bccf423f2d1
Step 4/5 : RUN go build -o ttt_api api/tictactoe/main.go
 ---> Running in 736b7e53d378
Removing intermediate container 736b7e53d378
 ---> dbdfee63a57b
Step 5/5 : CMD ./ttt_api
 ---> Running in 2ab074946bdb
Removing intermediate container 2ab074946bdb
 ---> cc6fc41dd71d
Successfully built cc6fc41dd71d
Successfully tagged tictactoe:latest
#+END_EXAMPLE

Nice! Now we can run the image on a container using:

#+begin_src sh
docker run -p 4000:8080 tictactoe
#+end_src

The flag =-p= specifies a "port mapping". Port =4000= of the host
machine is mapped to port =8080= of the container. This is because
my application listens on the port =8080=.

Done! We have a Dockerized tictactoe API server! 🏄
