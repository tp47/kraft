#+title: About
#+subtitle: What's the point of this blog?

Hi! You can call me /Thejeswi/ or /Preetham/, as you prefer.
I am a programmer who has worked with natural language processing
 and web development among other things.
I'm currently doing a [[https://apprenticeship.holidaycheck.com/][Apprenticeship at HolidayCheck]].
I loved to use Python and JavaScript to get my tasks done,
now enjoying working with Golang.

This blog is intended to be a hybrid of my personal experience
about learning software craftsmanship and technical notes.
You can access some of my projects on [[https://gitlab.com/tp47][Gitlab]], [[https://github.com/thejeswi][Github]], or read about them on
[[https://thejeswi.github.io/blog][my other blog]].
